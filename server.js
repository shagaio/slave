var config = require('config');
var express = require('express');
var path = require('path');
var app = express();
var bodyParser = require('body-parser');
var session = require('express-session');
var loggly =  require('loggly').createClient(config.loggly);
var db = require("then-redis").createClient(config.redis);
app.use(express.static(path.join(__dirname, './client')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}))

app.all("/*", function(req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.options("*", function (req, res, next){
    res.send(200);
});
var parser = require("mutil").parser();
app.post("/test/", function (req, res, next){
    var data = req.body;
    if (!data.url){
        return res.send(500, { message : "url required"});
    }
    if (!data.config){
        return res.send(500, { message : "config required"});
    }
    console.log(data.config.fields);
    var uri = data.url.toURL();
    if (!uri){
        return res.send(500, { message : "invalid uri required"});
    }
    uri.parse()
    .then(function ($){
        var result = parser.parse($, uri, [data.config]);
        res.send(result);
    })
    .catch(function (error){
        console.error(error);
        res.send(500, error);
    })
    
});
var http = require('http');
var server = http.createServer(app);
server.listen(process.env.PORT, process.env.IP, function() {
    console.log("Server started...");
});