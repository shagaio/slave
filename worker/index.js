var kue = require('kue');
var config = require("config");
var cluster = require('child_process');

var db = require("then-redis").createClient(config.redis);

var util = require("util");
var EventEmitter = require("events").EventEmitter;

var jobs = kue.createQueue(config.kue);

function updateStatus(id, status) {
    
    console.log(status);
    db.publish(config.kue.prefix + ":events", [JSON.stringify({
        id      : id,
        event   : status,
        args    : [status, "task"]
    })]);
}

function addTask(task, done) {
    var subscribe = require("then-redis").createClient(config.redis);
    var firebase = new(require("firebase-client"))(config.firebase);
    subscribe.subscribe("tasks:" + task.id);
    updateStatus(task.id, "started");
    var fork = cluster.fork("task/" + task.data.type + ".js", [JSON.stringify(task.data)]);
    fork.on("exit", function(code, signal) {
        console.log(code, signal)
        if (code && code != 143){
            return done({ code : code, signal : signal});
        }
        done();
        subscribe._redisClient.end();
    });
    fork.on('message', function(data) {
        db.publish(config.kue.prefix + ":events", [JSON.stringify({
            id      : task.id,
            event   : "message",
            args    : ["active", "task"],
            data    : data
            
        })]);
        console.log("task :", task.id, "stdout:", data);
    })
    subscribe.on("message", function(channel, message) {
        message = JSON.parse(message);
        console.log(channel, message, message.command);
        if (message.command == "TERMINATE") {
            fork.kill();
            updateStatus(task.id, "terminated");
        }
    });
    
}
console.log("start listening on queue", process.argv[2]);
jobs.process(process.argv[2], process.argv[3], function(task, done) {
    console.log(task.id, task.state());
    addTask(task, done);
});
