var util = require("util");
var EventEmitter = require("events").EventEmitter;
var puller = require("./puller.js")
var A = function A(config) {
    this.className = "classA";
    this.on("test", function (){
        console.log(this.className);
    })
}
util.inherits(A, EventEmitter);


module.exports = A;