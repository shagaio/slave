var Promise = require("promise");
var _ = require("underscore");
var config = require("config");
var db = require("then-redis").createClient(config.redis);
var found = require("mutil").queue(db, "found");
var params = require("./params.js");
var crawler = new(require("mutil").crawler)(params.read, 3, db);
var indexer = require("mutil").indexer(params.index);
crawler.start();
crawler.on("parsed", function($, uri) {
    var data = crawler.parser.parse($, uri);
    // console.log(data);
    var links = crawler.parser.links($, uri);
    // console.log(uri.toString());
    process.send({
        message: uri.toString()
    });
    
    
    data.objectID = uri.hash();
    data.link = uri.toString();
    
    Promise.all([
        found.push(links),
        indexer.index(data)
    ])
    .then(function(data) {
        crawler.emit("done", uri);
        console.log("success", data);
    })
    .catch(function(error) {
        console.error("error", error);
    })
})
crawler.on("close", function() {
    db._redisClient.end();
    // console.log("close");
});

process.on("uncaughtException", function (error){
    console.error(error)
    process.send({
        error: error
    })
})