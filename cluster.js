var config = require('config');
var cluster = require('cluster');
var loggly = require('loggly').createClient(config.loggly);
var firebase = new(require("firebase-client"))(config.firebase);
if (cluster.isMaster) {
    cluster.setupMaster({
        exec: "worker/index.js",
        args: ["task", "1"]
    });
    cluster.on('exit', function(worker, code, signal) {
        if (worker.suicide === true) {
            loggly.log('Oh, it was just suicide\' – no need to worry');
            return;
        }
        cluster.fork();
    });
    var cpus = require('os').cpus().length;
    firebase.push("nodes", {
        started  : new Date(),
        threads  : cpus
    }).then(function(data) {
        console.log(data);
        setInterval(function() {
            firebase.set("nodes/" + data.name + "/updated", new Date());
        }, 1000)
        process.on("exit", function() {
            console.log("test");
        })
    })
    for (var i = 0; i < cpus; i++) {
        cluster.fork();
    }
}