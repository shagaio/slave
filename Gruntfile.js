module.exports = function(grunt) {
    grunt.loadNpmTasks('grunt-git-deploy');
    grunt.loadNpmTasks('grunt-run');
    var stop = "";
    var start = "";
    ["slave", "slave-2", "zeus-1", "zeus-2", "zeus-3", "zeus-4", "zeus-5", "zeus6", "zeus7", "zeus8", "zeus9", "zeus10"].map(function(node) {
        stop += 'heroku scale worker=0 --app ' + node + ' & ';
        start += 'heroku scale worker=1 --app ' + node + ' & ';
    });
    
    grunt.initConfig({
        git_deploy: {
            your_target: {
                options: {
                    url: 'git@bitbucket.org:shagaio/slave.git',
                    branch: 'master'
                },
                src: '.'
            },
        },
        run: {
            stop: {
                exec : stop
            },
            start: {
                exec : start
            }
            // function() {

                // return ["slave", "slave-1", "zeus-1", "zeus-2", "zeus-3", "zeus-4", "zeus-5"].map(function(node) {
                //     console.log(node);
                //     return {
                //         exec: 'heroku scale worker=0 --app ' + node
                //     };
                // });
                // return {
                //     exec: 'heroku scale worker=0 --app ' + node
                // };
            // }
        }
    })
}