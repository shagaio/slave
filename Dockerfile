# Dockerfile used to build our wetopi/node apps

# Pull base image from an official repo
FROM node:0.12

MAINTAINER shagai

# Install
ADD package.json /src/package.json
RUN cd /src && npm install

# App
ADD . /src


# Remember the env:
# lets read development configs
# ENV NODE_ENV development
# lets node show us all debug info
# ENV DEBUG *

CMD [ "npm", "start" ]