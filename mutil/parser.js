var _ = require("underscore");
var api = {
    "default": {
        fields: [{
            name: "title",
            selector: "title",
            type: 3,
            value: {
                name: "text",
            }
        }, {
            name: "links",
            selector: "a",
            isCollection: true,
            value: {
                name: "attr",
                arg: "href"
            }
        }, {
            name: "images",
            selector: "img",
            isCollection: true,
            value: {
                name: "attr",
                arg: "src"
            }
        }]
    },
    
    find: function(uri, settings) {
        settings = settings || this.settings;
        return _.find(settings, function(setting) {
            if (setting.subdomain === "*"){
                return setting.domain === uri.domain();
            }
            return setting.domain === (uri.subdomain() + "." + uri.domain());
        });
    },
    list: function($, selector) {
        var data = []
        $(selector).each(function() {
            data.push($(this));
        })
        return data;
    },
    links: function($, uri) {
        return api.list($, "a").map(function(tag) {
            if (!tag.attr("href")) {
                return;
            }
            return tag.attr("href").toURL(uri.toString());
        }).compact()
    },
    extract: function(data, dom) {
        if (data.index > -1) {
            dom = dom.eq(data.index);
        }
        if (data.type) {
            dom = dom.contents().filter(function() {
                return this.nodeType === data.type;
            })
        }
        var result = dom[data.value.name](data.value.arg);
        if (result){
            result = result.replace(/(?:\r\n|\r|\n|\t)/g, '').trim();
        }
        return result;
    },
    parse: function($, uri, settings) {
        var config = this.find(uri, settings);
        config = config || api.default;
        var data = config.fields.map(function(data) {
            var obj = {};
            var dom = $(data.selector);
            if (data.isCollection) {
                
                obj[data.name] = [];
                dom.each(function() {
                    obj[data.name].push(api.extract(data, $(this)));
                });
            }
            else {
                obj[data.name] = api.extract(data, dom);
            }

            return obj;
        })

        var obj = {};
        data.map(function(data) {
            _.extend(obj, data);
        })
        return obj;
    }
}

module.exports = function (settings){
    api.settings = settings;
    return api;   
}