var events = require('events');
var _ = require("underscore");
var EventEmitter = require("events").EventEmitter;
var Puller = function (config){
    
    var queue = require("mutil").queue(config.db, config.queue);
    
    var _event = new events.EventEmitter();
    var end = false;
    var pool = {
        size : 0
    }
    _event.on("pop", function() {
        queue.pop().then(function(data) {
            if (!data) {
                _event.emit("end");
                return;
            }
            pool.size++;
            _event.emit("process", data, 0);
        });
    })
    .on("done", function(data) {
        pool.size--;
        if (end){
            _event.emit("close");
            return;
        }
        _event.emit("pop");
    })
    .on("error", function (data, attempt){
        attempt = attempt || 0;
        if (attempt < config.attempt){
            _event.emit("process", data, attempt + 1);
        }
    })
    .on("start", function (){
        _event.emit("pop");
    })
    .on("end", function (){
        end = true;
        if (pool.size == 0){
            _event.emit("close");
        }
    })
    .on("close", function (){
        config.db._redisClient.end();
    })
    return _event;
}

module.exports = Puller;