var util = require("util");
require("./url.js");
var config = require("config");
var _ = require("underscore");
var firebase = new(require("firebase-client"))(config.firebase);
var Crawler = function (queue, attempt, db){
    config.queue = queue ||  "default";
    config.attempt = attempt || 1;
    config.db = db;
    var puller = require("./puller.js")(config);
    puller.on("process", function (root, attempt){
        var uri = root.toURL();
        process.send({
            message : "cheking " + uri
        })
        if (!uri){
            return puller.emit("error", root, attempt);
        }
        uri.parse().then(function($) {
            process.send({
                message : "downloaded " + uri
            })
            puller.emit("parsed", $, uri);
        })
        .catch(function(error) {
            process.send({
                error : root + " " + attempt + " " + error.stack
            })
            puller.emit("error", root, attempt);
        })
    });
    puller.start = function (){
        firebase.get("parser").then(function(parsers) {
            puller.parser = require("mutil").parser(_.values(parsers));
            console.log(puller.parser);
            puller.emit("start");
        })
        .catch(function (error){
            console.error(error);
        })
    }
    puller.done = function(uri){
        uri = uri || "";
        puller.emit("done", uri.toString());
    }
    return puller;
}

module.exports = Crawler;