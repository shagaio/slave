var Promise = require("promise");
var URI = require("URIjs");
var request = require("request");
var cheerio = require("cheerio");
var fs = require("fs");
String.prototype.toURL = function(parent) {
    try {
        var uri = URI(this.toString());
        if (parent) {
            uri = uri.absoluteTo(parent);
        }
    
        uri = uri.normalize();
        uri = uri.normalizeHash();
        uri = uri.toString();
        uri = uri.replace(/jsessionid=.*?(?=\?|$)/g, "");
        uri = uri.replace(/phpsessid=.*?(?=\?|$)/g, "");
        var index = uri.indexOf("#");

        if (index > 0) {
            uri = uri.substring(0, index);
        }
        uri = URI(uri);
        return uri;
    }
    catch (e) {
        console.log(e);
        return;
    }
}

URI.prototype.hash = function() {
    return this.toString().hash();
}

URI.prototype.download = function() {
    var uri = this.toString();
    return new Promise(function (resolve, reject){
        request.get({
            uri: uri,
            headers: {
                "User-Agent": "Mozilla/5.0"
            }
        }, function (error, response, body){
            if (error){
                return reject(error);
            }
            resolve(body);
        });
    })
    
    
}

URI.prototype.parse = function() {
    return this.download().then(function(buffer) {
        return buffer.toString().parse();
    });
}

URI.prototype.save = function(location) {
    return this.download().then(function(buffer) {
        return new Promise(function(resolve, reject) {
            fs.writeFile(location, buffer, function(error, data) {
                if (error) {
                    return reject(error);
                }
                resolve(data);
            })
        })
    })
}