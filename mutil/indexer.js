var config = require("config");
var client = new(require('algolia-search'))(config.algolia.project, config.algolia.secret);
var Promise = require("promise");
module.exports = function(name) {
    var index = client.initIndex(name);
    return {
        index: function(data) {
            console.log(data);
            return new Promise(function (resolve, reject){
                index.addObject(data, function(error, content) {
                    if (error) {
                        return reject(error);
                    }
                    resolve(content);
                });
            })
        }
    }
}