var crypto = require('crypto');

var Promise = require("promise");
var URI = require("URIjs");
var request = require("request-promise");
var cheerio = require("cheerio");
var fs = require("fs");
var _ = require("underscore");

var url = require("./url.js");

Array.prototype.uniq = function() {
    return _.uniq(this);
}
Array.prototype.compact = function() {
    return _.compact(this);
}
String.prototype.hash = function() {
    return crypto.createHash('md5').update(this.toString()).digest("hex");
}
String.prototype.parse = function() {
    return cheerio.load(this.toString());
}

String.prototype.isAllowed = function() {
    console.log(this.toString());
}

module.exports = {
    queue   : require("./queue.js"),
    parser  : require("./parser.js"),
    crawler : require("./crawler.js"),
    slave   : require("./slave.js"),
    indexer : require("./indexer.js")
}
