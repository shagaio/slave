var Promise = require("promise");
var crypto = require('crypto');
var _ = require("underscore");
module.exports = function (db, name){
    if (!name){
        name = "links";
    }
    var api = {
        clean: function (){
            return Promise.all([
                db.send('del', [name + ":list"]),
                db.send('del', [name + ":set"])
            ]) 
        },
        hash: function (data){
            return data.hash();
        },
        pop : function (){
            return db.send('RPOP', [name + ":list"]).then(function (data){
                if (data){
                    return data;
                }
                return;
            })
        },
        build: function (data){
            if (!_.isArray(data)){
                var map = {};
                map[api.hash(data)] = data;
                return map;
            }
            return _.object(data.map(api.hash), data);
        },
        push : function (data){
            var map = api.build(data);
            return db.send('SDIFF', [name + ":set"].concat(_.keys(map))).then(function (value) {
                map = _.omit(map, value);
                if (_.isEmpty(map)){
                    return [0, 0];
                }
                return Promise.all([
                    db.send('SADD', [name + ":set"].concat(_.keys(map))),
                    db.send('LPUSH', [name + ":list"].concat(_.values(map)))
                ]);
            });
        },
        length : function (){
            return db.send('LLEN', [name + ":list"]);
        },
        end : function (){
            db._redisClient.end();
        }
        
        
    }
    
    return api;
}